import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws DocumentExistExeption {
        ArrayList<Document> docs=new ArrayList<Document>();
        int incomingId=1, outgoingId=1,taskId=1;
        for (int i=0; i<6; i++){
            Document doc = generateDocuments(DocumentType.randomDocType());
            docs.add(doc);
        }
        Map<String, Set<Document>> m=docs.stream()
                .collect(Collectors.groupingBy(Document::getAuthorOfDocument, Collectors.toSet()));
        ArrayList<Set<Document>> groupedByAuthor = new ArrayList<>( m.values());
        for (Set<Document> o: groupedByAuthor
             ) {
                List<Document> sortedList=new ArrayList<>(o);
                Collections.sort(sortedList, new Comparator<Document>() {
                    public int compare(Document o1, Document o2) {
                        String className1, className2;
                        className1=getClassName(o1);
                        className2=getClassName(o2);
                        return className1.compareTo(className2);
                    }
                });
                for (Document document:sortedList
                     ) {
                    if(document==o.toArray()[0]) System.out.println(document.authorOfDocument);
                    if(document.getClass()==Incoming.class){
                        document.setIdOfDocument(incomingId);
                        System.out.println("    Входящий №"+document.idOfDocument+" от "+document.regDateOfDocument+ " "+ document.nameOfDocument);
                        incomingId++;}
                    if(document.getClass()==Task.class) {
                        document.setIdOfDocument(taskId);
                        System.out.println("    Поручение №" + document.idOfDocument + " от " + document.regDateOfDocument + " " + document.nameOfDocument);
                        taskId++;
                    }
                    if(document.getClass()==Outgoing.class) {
                        document.setIdOfDocument(outgoingId);
                        System.out.println("    Исходящий №" + document.idOfDocument + " от " + document.regDateOfDocument + " " + document.nameOfDocument);
                        outgoingId++;
                    }
                }
        }
    }
public static String getClassName(Document o){
        String className=null;
    if(o.getClass()==Incoming.class)  className="Входящий";
    if(o.getClass()==Outgoing.class)  className="Исходящий";
    if(o.getClass()==Task.class)  className="Поручение";
    return className;
}
    public static Document generateDocuments(DocumentType type) throws DocumentExistExeption {
        Fabric docFactory= new Fabric();
        Document doc =  docFactory.factoryMethod(type);
        return doc;
    }
}
