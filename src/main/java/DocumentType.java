import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum DocumentType {
    TASK,
    INCOMING,
    OUTGOING;

    private static final List<DocumentType> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static DocumentType randomDocType()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
