import java.util.ArrayList;

public class Fabric {
    ArrayList<Integer> usedRegNums;
    ArrayList<String> nameOfDoc=new ArrayList<String>();
    ArrayList<String> authorsOfDoc=new ArrayList<String>();
    ArrayList<String> regDateOfDoc=new ArrayList<String>();
    ArrayList<String> textOfDoc=new ArrayList<String>();
    public void setValuesOfDoc(ArrayList<String> valuesOfDoc, String[] values) {
        for (int i = 0; i < values.length; i++){
            valuesOfDoc.add(values[i]);
        }
    }
    public Document factoryMethod(DocumentType type) throws DocumentExistExeption {
        Document doc= null;
        usedRegNums=new ArrayList<Integer>();
        setValuesOfDoc(nameOfDoc, new String[]{"Доклад","Важное сообщение","Пояснительная записка",
                "Готовая работа","Личное сообщение","Задание"});
        setValuesOfDoc(authorsOfDoc,new String[]{"Мустафин Радмир","Родин Данил",
                "Грабовенко Мария","Васильев Арсентий","Юдинцева Ольга"});
        setValuesOfDoc(regDateOfDoc,new String[]{"18/01/2021","20/01/2021",
                "14/02/2021","08/03/2021","26/02/2021"});
        setValuesOfDoc(textOfDoc,new String[]{"Хотя в языке C# есть массивы, которые хранят в себе наборы однотипных объектов, но работать с ними не всегда удобно.",
                "Например, массив хранит фиксированное количество объектов, однако что если мы заранее не знаем, сколько нам потребуется объектов.",
                "И в этом случае намного удобнее применять коллекции.",
                "Еще один плюс коллекций состоит в том, что некоторые из них реализует стандартные структуры данных, например, стек, очередь, словарь, которые могут пригодиться для решения различных специальных задач.",
                "Большая часть классов коллекций содержится в пространствах имен System.Collections (простые необобщенные классы коллекций), System.Collections.Generic (обобщенные или типизированные классы коллекций) и System.Collections.Specialized (специальные классы коллекций)."});
        switch (type) {
            case TASK:
                doc = new Task();
                doc=makeTask((Task) doc);
                break;
            case INCOMING:
                doc = new Incoming();
                doc=makeIncoming((Incoming)doc);
                break;
            case OUTGOING:
                doc = new Outgoing();
                doc=makeOutgoing((Outgoing)doc);
                break;
        }

        return doc;
    }

    public Document makeTask(Task task) throws DocumentExistExeption {
        int regNum=generateRegNums();
        usedRegNums.add(regNum);
        task.regNumOfDocument=regNum;
        task.nameOfDocument=nameOfDoc.get(generateNumber(nameOfDoc.size()));
        task.regDateOfDocument=regDateOfDoc.get(generateNumber(regDateOfDoc.size()));
        task.authorOfDocument=authorsOfDoc.get(generateNumber(authorsOfDoc.size()));
        task.textOfDocument=textOfDoc.get(generateNumber(textOfDoc.size()));
        return task;
    }
    public Document makeIncoming(Incoming message) throws DocumentExistExeption {
        int regNum=generateRegNums();
        usedRegNums.add(regNum);
        message.regNumOfDocument=regNum;
        message.nameOfDocument=nameOfDoc.get(generateNumber(nameOfDoc.size()));
        message.regDateOfDocument=regDateOfDoc.get(generateNumber(regDateOfDoc.size()));
        message.authorOfDocument=authorsOfDoc.get(generateNumber(authorsOfDoc.size()));
        message.textOfDocument=textOfDoc.get(generateNumber(textOfDoc.size()));
        return message;
    }
    public Document makeOutgoing(Outgoing message) throws DocumentExistExeption {
        int regNum=generateRegNums();
        usedRegNums.add(regNum);
        message.regNumOfDocument=regNum;
        message.nameOfDocument=nameOfDoc.get(generateNumber(nameOfDoc.size()));
        message.regDateOfDocument=regDateOfDoc.get(generateNumber(regDateOfDoc.size()));
        message.authorOfDocument=authorsOfDoc.get(generateNumber(authorsOfDoc.size()));
        message.textOfDocument=textOfDoc.get(generateNumber(textOfDoc.size()));
        return message;
    }
public int generateNumber(int endNumber){
        return (int)(Math.random()*endNumber);
}
    public int generateRegNums() throws DocumentExistExeption {
        int regNum=0;
        regNum= 100+ (int)(Math.random()*10000);
        if(usedRegNums.contains(regNum)==true) throw new DocumentExistExeption("Документ с таким номером уже существует");
        return regNum;
    }}

class DocumentExistExeption extends Exception{
    public DocumentExistExeption(String message){
        super(message);
    }}
