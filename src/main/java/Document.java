import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public abstract class Document implements Storable{
    public int idOfDocument;
    public String nameOfDocument;
    public String textOfDocument;
    public int regNumOfDocument;
    public String regDateOfDocument;
    public String authorOfDocument;

    public void setIdOfDocument(int id){
        this.idOfDocument=id;
    }
    public int getIdOfDocument(){
        return idOfDocument;
    }

    public void setNameOfDocument(String name){
        this.nameOfDocument=name;
    }
    public String getNameOfDocument(){
        return nameOfDocument;
    }

    public void setTextOfDocument(String text){
        this.textOfDocument=text;
    }
    public String getTextOfDocument(){
        return textOfDocument;
    }
    public void setRegNumOfDocument(int regNum){
        this.regNumOfDocument=regNum;
    }
    public int getRegNumOfDocument(){
        return regNumOfDocument;
    }
    public void setRegDateOfDocument(String date){
        this.regDateOfDocument=date;
    }
    protected String getRegDateOfDocument(){
        return regDateOfDocument;
    }
    protected void setAuthorOfDocument(String author){
        this.authorOfDocument=author;
    }
    protected String getAuthorOfDocument(){
        return authorOfDocument;
    }
    Document(){
    }
    Document(int id, String name, String text, int regNum, String date, String author){
        this.idOfDocument=id;
        this.nameOfDocument=name;
        this.textOfDocument=text;
        this.regNumOfDocument=regNum;
        this.regDateOfDocument=date;
        this.authorOfDocument=author;
    }

    @Override
    public int getDocumentID() {
        return getIdOfDocument();
    }

    @Override
    public String dataStoreName() {
        return null;
    }
}
class documentRegNumComparator implements Comparator<Document> {

    public int compare(Document a, Document b){

        return a.regNumOfDocument>b.regNumOfDocument?1:0;
    }
}
class documentRegDateComparator implements Comparator<Document> {

    public int compare(Document a, Document b) {
        Date date1 = null, date2= null;
        try {
            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(a.regDateOfDocument);
            date2 = new SimpleDateFormat("dd/MM/yyyy").parse(b.regDateOfDocument);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1.compareTo(date2);
    }
}
interface Storable {
int getDocumentID();
String dataStoreName();
}

