public class Incoming extends Document {
    protected String senderOfIncoming;
    protected String destinationOfIncoming;
    protected int outgoingNumberOfIncoming;
    protected String outgoingRegistrationDateOfIncoming;
    void textToString(){
        System.out.println(Incoming.super.getTextOfDocument());
    }
Incoming(){
}
Incoming(String senderOfIncoming, String destinationOfIncoming, int outgoingNumberOfIncoming, String outgoingRegistrationDateOfIncoming){
        this.senderOfIncoming=senderOfIncoming;
        this.destinationOfIncoming=destinationOfIncoming;
        this.outgoingNumberOfIncoming=outgoingNumberOfIncoming;
        this.outgoingRegistrationDateOfIncoming=outgoingRegistrationDateOfIncoming;
}
    public String getOutgoingRegistrationDateOfIncoming() {
        return outgoingRegistrationDateOfIncoming;
    }

    public void setOutgoingRegistrationDateOfIncoming(String outgoingRegistrationDateOfIncoming) {
        this.outgoingRegistrationDateOfIncoming = outgoingRegistrationDateOfIncoming;
    }

    public int getOutgoingNumberOfIncoming() {
        return outgoingNumberOfIncoming;
    }

    public void setOutgoingNumberOfIncoming(int outgoingNumberOfIncoming) {
        this.outgoingNumberOfIncoming = outgoingNumberOfIncoming;
    }

    public String getDestinationOfIncoming() {
        return destinationOfIncoming;
    }

    public void setDestinationOfIncoming(String destinationOfIncoming) {
        this.destinationOfIncoming = destinationOfIncoming;
    }

    public String getSenderOfIncoming() {
        return senderOfIncoming;
    }

    public void setSenderOfIncoming(String senderOfIncoming) {
        this.senderOfIncoming = senderOfIncoming;
    }
}
